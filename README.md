# Responsive website layout

## Tạo giao diện đáp ứng các kích cỡ màn hình khác nhau

![Full page](images/mobile.png)

## 🧱 Technology

- Front-end:

  - Bootstrap 5
  - Javascript
  - Owl carousel